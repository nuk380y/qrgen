# QRgen

`qrgen` outputs a QR Code (v10) with data formatted to various purposes.


## Usage

```
Usage: qrgen -o output -s ssid [OPTIONS]

Options:
  -o <output>       Filename and relative path for output file
  -s <ssid>         SSID for network
  -l <logo>         Filename and relative path for logo image
  -b <background>   Filename and relative path for background image
  -p <password>     Password/passphrase for network
  --hide            Toggle accessability for hidden SSID (default "false").
```


## Dependencies

`qrgen` requires the following programs in order to function:
  - qrencode
  - ImageMagick


## Notes

For the optional logo and background images, it is strongly recommended to
only use files in PNG format.  For best results, a logo should be
monochromatic with a white outline and trimmed, whereas a background should
avoid larger areas of white coloring as QR codes define the code by areas of
contrast.


## License

MIT
