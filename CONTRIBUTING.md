# How to contribute

This project is accepting commits from all skill levels, and hopefully there'll
be issues to reflect that.  There are some simple standards for scripting and
documentation that should be taken into account when preparing merge requests
(MRs).  Read on for the MR standards while waiting for the maintainer to figure
out how to properly integrate CI/CD pipelines with a free account.


## Basic Guidance for Contributing

While following these guidelines won't guarantee that your MR is accepted, they
will help avoid much headache on behalf of both you and the review.

- Fork the repository
  - Submit from feature branches
- Spellcheck before pushing
- Each indentation should be two (2) space characters
- Lines should not extend past 80 characters


## Check for Issues

While this project is developing, there is a general plan for where this
project is trying to go.  Check what issues are currently available and if any
issues in the "In Progress" column have a "Help Wanted" label, verify you
understand all **Acceptance Criteria** for the parent ticket and coordinate
with any existing contributors if applicable.


## Issue Workflow

In order to maintain workflow and keeping improvements incremental, please only
pull issues with the "Selected for Development" label.  Assign them to the
account the MR will be coming from and change the "Selected for Development"
label to "In Progress".  This is a drag and drop process if using the board
view.

Should something be preventing you from making additional progress on an issue
you are working on, change the label to "Blocked" and leave a comment on the
issue to either _(a)_ get outside input, or _(b)_ push a blocking issue higher
in priority.  In the latter case, include the issue number in the comment on
the blocked issue.

After submitting your MR, change the label to "In Review" and work with the
reviewer to ensure standards are upheld.

### Bug Issues

Bugs issues are the only issue types that can be pulled without the "Selected
for Development" label.  Any MRs submitted to affect a bug _must first_ have
a corresponding Bug issue, and the MR _must_ have that issue tagged.  All other
issue workflow standards still apply.


## Issue Standards

### Definition of Ready

For issues to be considered for the "Selected for Development" label, they need
to meet the following criteria:

- Title is a succinct summary of the end goal.
- Description elaborates on the title.
- There is an **Acceptance Criteria** section describing expected changes from
  any relevant MRs.
- In cases where issues relate to others, add the relationship to a **NOTES**
  section after the **Acceptance Criteria** with a reference to the related
  issue.
  - It is preferred that any linked issues in **NOTES** also a short
    description of the relationship between the issues. (i.e. "Blocker to #")


## Reviewing Standards

Reviewers need to approach reviews as a mentoring opportunity.  Try to keep all
in-line comments in a single review to try and respect the time of other
contributors.  While the maintainer cannot monitor all reviews, any reports of
inappropriate behavior from reviewers will be investigated.

Should any reports of inappropriate reviews be proven as factual, the offending
reviewer will be blocked from the project.  We need to look after all
contributors, and it seems just that erroneous reports of inappropriate reviews
will see the submitter of the report blocked.
